window.onload=init;
function init(){
    accueilPerso();
    document.getElementById('nuit').addEventListener('click',modeNuit);
    document.getElementById('deconnexion').addEventListener('click',deco);
    logout();
}


function logout() {
    var btn = document.getElementById("logout-btn");
    btn.classList.add("loading");
    sessionStorage.removeItem('user');
    setTimeout(function() {
      btn.classList.remove("loading");
      window.location.href='../pagesHtml/connection.html';
    }, 2000); 
  }

  
  

function modeNuit(){
    if(document.getElementById('nuit').getAttribute('class')=='fa-solid fa-toggle-on fa-2xl'){
        document.getElementById('nuit').setAttribute('class','fa-solid fa-toggle-off fa-2xl')
        document.getElementById('nuit').style.color='#ffd700'
        document.body.style.background ='rgb(48, 48, 48)'
        document.body.style.color ='gold'
        document.querySelector('nav').style.background ='rgb(48, 48, 48)'
        document.querySelector('nav').style.borderBottom ='solid','gold','5px'
    }else{
        document.getElementById('nuit').setAttribute('class','fa-solid fa-toggle-on fa-2xl')
        document.getElementById('nuit').style.color='rgb(48, 48, 48)'
        document.body.style.background ='white'
        document.body.style.color ='rgb(48, 48, 48)'
        document.querySelector('nav').style.background ='white'
        document.querySelector('nav').style.borderBottom ='solid','rgb(48, 48, 48)','5px'
        document.querySelector('li').style.border ='solid','rgb(48, 48, 48)','5px'  
    }  
}
function accueilPerso(){
    let user = JSON.parse(sessionStorage.getItem('user'));
    document.getElementById('acceuilUser').innerText='Bienvenue '+user.nom;
    document.getElementById('mail').innerText='email :'+user.mail;
    let nbcps =localStorage.getItem('victoires')
    let tableauScore = [];
    tableauScore.push(nbcps);
    console.log(tableauScore)
    for(let i=0; i<tableauScore.length; i++){
         document.getElementById('score').innerHTML = "<div id='ligneScore"+i+"'><p id='pScore'>Nombre de coups :"+tableauScore[i]+"</p></div>"
    }
}
