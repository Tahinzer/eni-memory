window.onload=init;
function init(){
    document.getElementById('nuit').addEventListener('click',modeNuit);
}
function modeNuit(){
    if(document.getElementById('nuit').getAttribute('class')=='fa-solid fa-toggle-on fa-2xl'){
        document.getElementById('nuit').setAttribute('class','fa-solid fa-toggle-off fa-2xl')
        document.getElementById('nuit').style.color='#ffd700'
        document.body.style.background ='rgb(48, 48, 48)'
        document.body.style.color ='gold'
        document.querySelector('nav').style.background ='rgb(48, 48, 48)'
        document.querySelector('nav').style.borderBottom ='solid','gold','5px'
        localStorage.setItem('mode', 'nuit')
    }else{
        document.getElementById('nuit').setAttribute('class','fa-solid fa-toggle-on fa-2xl')
        document.getElementById('nuit').style.color='rgb(48, 48, 48)'
        document.body.style.background ='white'
        document.body.style.color ='rgb(48, 48, 48)'
        document.querySelector('nav').style.background ='white'
        document.querySelector('nav').style.borderBottom ='solid','rgb(48, 48, 48)','5px'
        document.querySelector('li').style.border ='solid','rgb(48, 48, 48)','5px'
        localStorage.setItem('mode', 'jour')  
    }  
}