window.onload=init;

window.scroll({
        top: 1000,
        left :0,
        behavior : 'smooth'
    }); // marche plus avec le grid dans le css
let placeTire = new Set();
let cpt=0;
let carteMemoire;
let stockage;
const tableauComparaison =Array();
let cptPaire =0;
let nbDeCoups =0;
let nomDomaine = '.svg';
let nomDossier = 'memory-legume';
let depart = false;


function init(){
    choixDesPlaces();
    window.addEventListener('keydown',rafraichir)    
    document.getElementById('nuit').addEventListener('click',modeNuit);
    affichage(); 
    document.getElementById('choix').addEventListener('input',changerCarte);    
}

function changerCarte(){
    let choix =document.getElementById('choix').value;
    switch (choix){
        case '0' : 
            nomDomaine = '.png';
            nomDossier ='alphabet-scrabble';
            break;
        case '1' :
            nomDomaine = '.webp';
            nomDossier ='animaux';
            break;
        case '2' :
            nomDomaine = '.webp';
            nomDossier ='animauxAnimes';
            break;
        case '3' :
            nomDomaine = '.jpg';
            nomDossier ='animauxdomestiques';
            break;
        case '4' :
            nomDomaine = '.webp';
            nomDossier ='chiens';
            break;
        case '5' :
            nomDomaine = '.jpg';
            nomDossier ='dinosaures';
            break;
        case '6' :
            nomDomaine = '.jpg';
            nomDossier ='dinosauresAvecNom';
            break;
        case '7' :
            nomDomaine = '.svg';
            nomDossier ='memory-legume';
            break;
        case '8' :
            nomDomaine = '.svg';
            nomDossier ='nDifficile';
            break;
    }
    document.getElementById('choix').style.pointerEvents='none'
    
}



function modeNuit(){
    if(document.getElementById('nuit').getAttribute('class')=='fa-solid fa-toggle-on fa-2xl'){
        document.getElementById('nuit').setAttribute('class','fa-solid fa-toggle-off fa-2xl')
        document.getElementById('nuit').style.color='#ffd700'
        document.body.style.background ='rgb(48, 48, 48)'
        document.body.style.color ='gold'
        document.querySelector('nav').style.background ='rgb(48, 48, 48)'
        document.querySelector('nav').style.borderBottom ='solid','gold','5px'
    }else{
        document.getElementById('nuit').setAttribute('class','fa-solid fa-toggle-on fa-2xl')
        document.getElementById('nuit').style.color='rgb(48, 48, 48)'
        document.body.style.background ='white'
        document.body.style.color ='rgb(48, 48, 48)'
        document.querySelector('nav').style.background ='white'
        document.querySelector('nav').style.borderBottom ='solid','rgb(48, 48, 48)','5px'
        document.querySelector('li').style.border ='solid','rgb(48, 48, 48)','5px'  
    }   
}
function rafraichir(event){
if(event.key == " "){
    window.location.reload();   
}
}   
//aleatoire pour le choix des places
function choixDesPlaces() {
    const MAX =12;
    while (placeTire.size < MAX){
        let alea = Math.trunc(Math.random()*MAX)+1;
        placeTire.add(alea);   
    }
}
//affiche les places
function affichage(){
    //tableau qui affiche les cartes visibles
    let tableauAffichage =  Array.from(placeTire)
    for(let j=0; j<tableauAffichage.length;j++){
        if (tableauAffichage[j]>6){
            tableauAffichage[j]=tableauAffichage[j]-6
        }
    }
    for(let i=0;i<12;i++){
            document.getElementById('place'+(i+1)).innerHTML = " <img id ='"+(i)+"'class='laCarte' src='../img/ressources/question.svg' alt=''>"           
    }
    let tagName =document.getElementsByClassName('laCarte')
    for(let j =0;j<tagName.length;j++){
     tagName[j].addEventListener('click',retourner);   
    }
    function retourner(event){
            nbDeCoups++
            event.currentTarget.style.transform ='rotateY(180deg)'
            event.currentTarget.style.transition ='all 0.3s ease'
            event.currentTarget.src="../img/ressources/"+nomDossier+"/"+(tableauAffichage[event.currentTarget.id])+nomDomaine;
            cpt++
            if (cpt ==1){
                stockage = event.currentTarget.id;
                 document.getElementById(stockage).style.pointerEvents='none'
            }else if(cpt==2){
                document.getElementById(stockage).style.pointerEvents='all'
                if(tableauAffichage[stockage]== tableauAffichage[event.currentTarget.id]){
                    tableauComparaison.push(stockage,event.currentTarget.id)
                    cptPaire++;
                    if(cptPaire > 5){
                    document.getElementById('principal').style.transform='rotateX(1080deg)'
                    document.getElementById('principal').style.transition='all 3s ease'
                    document.getElementById('victoire').style.fontSize = '50px'
                    document.getElementById('victoire').style.transition = 'all 3s ease'
                    let victoire ={
                        "nbcps" : Math.trunc((nbDeCoups/2))
                    }
                    let victoirePresent = localStorage.getItem('victoires');
                    if (victoirePresent){
                        let listVictoire = JSON.parse(victoirePresent);
                        listVictoire.push(victoire);
                        localStorage.setItem('victoires',JSON.stringify(listVictoire));
                    }else{
                        let victoireTab =[];
                        victoireTab.push(victoire);
                        localStorage.setItem('victoires',JSON.stringify(victoireTab));
                    }
                    }
                    cpt =0;
                    document.getElementById('nbcps').innerHTML="Nombre de coups : " + Math.trunc((nbDeCoups/2));
                    return;
                }else{
                    document.getElementById('zoneDeJeu').style.pointerEvents='none'
                    setTimeout(()=>{ 
                        for( let i=0;i<12;i++){
                            if( tableauComparaison.includes(String(i)) ){  
                            }else{
                                document.getElementById(i).src = '../img/ressources/question.svg';
                                document.getElementById(i).style.transform = 'rotateY(0deg)';
                                document.getElementById(i).style.transition ='all 0.3s ease';   
                            }
                            cpt =0; 
                             } 
                            
                            document.getElementById('zoneDeJeu').style.pointerEvents='all'
                    },1500);   
                }
            }
            document.getElementById('nbcps').innerHTML="Nombre de coups : " + Math.trunc((nbDeCoups/2));
        }  
    }
